# Open TimeLimit

This App allows setting time limits for the usage of Android phones/ devices.

It is a fork of the proprietary App [TimeLimit](https://timelimit.io)
with all networking related features removed.

[<img src="./get-it-on-fdroid-badge.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/app/io.timelimit.android.open)

### Building

Open it with Android Studio and press the Run button.

### Screenshots

![overview screen](./app/src/main/play/en-US/listing/phoneScreenshots/1.png)
![time limit rule screen](./app/src/main/play/en-US/listing/phoneScreenshots/2.png)
