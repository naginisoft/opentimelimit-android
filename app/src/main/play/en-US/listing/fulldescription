Flexible

Apps are grouped to categories (a category can contain one or multiple App).

You can chose per category at which times it should be allowed. This allows preventing playing games too late.

Additionally, you can configure time limit rules. These rules limit the total usage duration at one day or over multiple days (e.g. a weekend). It is possible to combine both, e.g. 2 hours per week end day, but in total only 3 hours.

Moreover, there is the possibility to set an extra time. This allows using something longer than regulary once. This can be used as bonus. There is additionally the option to disable all time limits temporarily (e.g. for the whole day or an hour).

Multi user support

There is the scenario that one device is used by exactly one user. However, with tablets, there are often multiple possible users. Due to that, it is possible to create multiple user profiles in TimeLimit. Each user has got different settings and time counters. There are two kinds of users: parents and children. If a parent was chosen as user, then there are no restrictions. Parents can chose any other user as current user.

Notes

If it "does not work": This can be caused by power saving features. You can find at https://dontkillmyapp.com/ how you can disable these features. Get in touch with the support if that does not help.

Depending on the Android version, TimeLimit uses the permission for the usage stats access or the GET_TASKS permission. These are only used to detect the currently used App. Based on the currently used App, the App is blocked, allowed, or the remaining time is calculated.

The device admin permission is used to detect an uninstallation of TimeLimit.

TimeLimit uses the notification access to block notifications of blocked apps and to terminate media players completely. Notifications and their contents are not saved.

TimeLimit uses an accessibility service to press the home button before showing the lock screen. This fixes blocking in some cases. Moreover, this allows opening the lockscreen at newer Android versions.

TimeLimit uses the permission "draw over other Apps" to allow opening the lockscreen at newer android versions and to overlay blocked Apps until the lockscreen is launched.
