<!--
    Open TimeLimit Copyright <C> 2019 Jonas Lochmann

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
-->
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    tools:context="io.timelimit.android.ui.setup.SetupDevicePermissionsFragment">

    <data>
        <variable
            name="usageStatsAccess"
            type="io.timelimit.android.integration.platform.RuntimePermissionStatus" />

        <variable
            name="notificationAccessPermission"
            type="io.timelimit.android.integration.platform.NewPermissionStatus" />

        <variable
            name="protectionLevel"
            type="io.timelimit.android.integration.platform.ProtectionLevel" />

        <variable
            name="overlayPermission"
            type="RuntimePermissionStatus" />

        <variable
            name="accessibilityServiceEnabled"
            type="boolean" />

        <variable
            name="handlers"
            type="io.timelimit.android.ui.setup.SetupDevicePermissionsHandlers" />

        <import type="android.view.View" />
        <import type="io.timelimit.android.integration.platform.RuntimePermissionStatus" />
        <import type="io.timelimit.android.integration.platform.NewPermissionStatus" />
        <import type="io.timelimit.android.integration.platform.ProtectionLevel" />
    </data>

    <ScrollView
        android:id="@+id/scroll"
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <LinearLayout
            android:padding="8dp"
            android:orientation="vertical"
            android:layout_width="match_parent"
            android:layout_height="wrap_content">

            <TextView
                android:text="@string/setup_device_permissions_text"
                android:textAppearance="?android:textAppearanceMedium"
                android:layout_width="match_parent"
                android:layout_height="wrap_content" />

            <androidx.cardview.widget.CardView
                android:onClick="@{() -> handlers.openUsageStatsSettings()}"
                android:foreground="?selectableItemBackground"
                android:clickable="@{usageStatsAccess != RuntimePermissionStatus.NotRequired}"
                app:cardUseCompatPadding="true"
                android:layout_width="match_parent"
                android:layout_height="wrap_content">
                <LinearLayout
                    android:orientation="vertical"
                    android:padding="8dp"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:textAppearance="?android:textAppearanceLarge"
                        android:text="@string/manage_device_permissions_usagestats_title"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permissions_usagestats_text"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{usageStatsAccess == RuntimePermissionStatus.Granted ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/green"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_granted"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{usageStatsAccess == RuntimePermissionStatus.NotGranted ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/red"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_not_granted"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{usageStatsAccess == RuntimePermissionStatus.NotRequired ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/green"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_not_required"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{usageStatsAccess != RuntimePermissionStatus.NotRequired ? View.VISIBLE : View.GONE}"
                        android:text="@string/manage_device_permission_tap_top_open_settings"
                        android:textAppearance="?android:textAppearanceSmall"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                </LinearLayout>
            </androidx.cardview.widget.CardView>

            <androidx.cardview.widget.CardView
                android:foreground="?selectableItemBackground"
                android:onClick="@{() -> handlers.manageDeviceAdmin()}"
                app:cardUseCompatPadding="true"
                android:layout_width="match_parent"
                android:layout_height="wrap_content">
                <LinearLayout
                    android:orientation="vertical"
                    android:padding="8dp"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:textAppearance="?android:textAppearanceLarge"
                        android:text="@string/manage_device_permission_device_admin_title"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{protectionLevel == ProtectionLevel.None ? View.VISIBLE : View.GONE}"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_device_admin_text_disabled"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{protectionLevel == ProtectionLevel.SimpleDeviceAdmin ? View.VISIBLE : View.GONE}"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_device_admin_text_simple"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{protectionLevel == ProtectionLevel.PasswordDeviceAdmin ? View.VISIBLE : View.GONE}"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_device_admin_text_password"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{protectionLevel == ProtectionLevel.DeviceOwner ? View.VISIBLE : View.GONE}"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_device_admin_text_owner"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:text="@string/manage_device_permission_tap_top_open_settings"
                        android:textAppearance="?android:textAppearanceSmall"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                </LinearLayout>
            </androidx.cardview.widget.CardView>

            <androidx.cardview.widget.CardView
                android:onClick="@{() -> handlers.openNotificationAccessSettings()}"
                android:foreground="?selectableItemBackground"
                android:clickable="@{notificationAccessPermission != NewPermissionStatus.NotSupported}"
                app:cardUseCompatPadding="true"
                android:layout_width="match_parent"
                android:layout_height="wrap_content">
                <LinearLayout
                    android:orientation="vertical"
                    android:padding="8dp"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:textAppearance="?android:textAppearanceLarge"
                        android:text="@string/manage_device_permission_notification_access_title"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_notification_access_text"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{notificationAccessPermission == NewPermissionStatus.Granted ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/green"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_granted"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{notificationAccessPermission == NewPermissionStatus.NotGranted ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/red"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_not_granted"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{notificationAccessPermission == NewPermissionStatus.NotSupported ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/red"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_not_supported"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{notificationAccessPermission != NewPermissionStatus.NotSupported ? View.VISIBLE : View.GONE}"
                        android:text="@string/manage_device_permission_tap_top_open_settings"
                        android:textAppearance="?android:textAppearanceSmall"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                </LinearLayout>
            </androidx.cardview.widget.CardView>

            <androidx.cardview.widget.CardView
                android:onClick="@{() -> handlers.openDrawOverOtherAppsScreen()}"
                android:foreground="?selectableItemBackground"
                android:clickable="@{usageStatsAccess != RuntimePermissionStatus.NotRequired}"
                app:cardUseCompatPadding="true"
                android:layout_width="match_parent"
                android:layout_height="wrap_content">
                <LinearLayout
                    android:orientation="vertical"
                    android:padding="8dp"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:textAppearance="?android:textAppearanceLarge"
                        android:text="@string/manage_device_permissions_overlay_title"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permissions_overlay_text"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{overlayPermission == RuntimePermissionStatus.Granted ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/green"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_granted"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{overlayPermission == RuntimePermissionStatus.NotGranted ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/red"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_not_granted"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{overlayPermission == RuntimePermissionStatus.NotRequired ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/green"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_not_required"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{overlayPermission != RuntimePermissionStatus.NotRequired ? View.VISIBLE : View.GONE}"
                        android:text="@string/manage_device_permission_tap_top_open_settings"
                        android:textAppearance="?android:textAppearanceSmall"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                </LinearLayout>
            </androidx.cardview.widget.CardView>

            <androidx.cardview.widget.CardView
                android:onClick="@{() -> handlers.openAccessibilitySettings()}"
                android:foreground="?selectableItemBackground"
                android:clickable="true"
                android:focusable="true"
                app:cardUseCompatPadding="true"
                android:layout_width="match_parent"
                android:layout_height="wrap_content">
                <LinearLayout
                    android:orientation="vertical"
                    android:padding="8dp"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:textAppearance="?android:textAppearanceLarge"
                        android:text="@string/manage_device_permission_accessibility_title"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_accessibility_text"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{accessibilityServiceEnabled == true ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/green"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_granted"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:visibility="@{accessibilityServiceEnabled == false ? View.VISIBLE : View.GONE}"
                        android:textColor="@color/red"
                        android:textAppearance="?android:textAppearanceMedium"
                        android:text="@string/manage_device_permission_status_not_granted"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <TextView
                        android:text="@string/manage_device_permission_tap_top_open_settings"
                        android:textAppearance="?android:textAppearanceSmall"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                </LinearLayout>
            </androidx.cardview.widget.CardView>

            <LinearLayout
                android:orientation="horizontal"
                android:layout_width="match_parent"
                android:layout_height="wrap_content">

                <View
                    android:layout_weight="1"
                    android:layout_width="0dp"
                    android:layout_height="0dp" />

                <Button
                    android:enabled="@{usageStatsAccess != RuntimePermissionStatus.NotGranted}"
                    android:onClick="@{() -> handlers.gotoNextStep()}"
                    android:text="@string/wiazrd_next"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content" />

            </LinearLayout>

        </LinearLayout>
    </ScrollView>
</layout>
